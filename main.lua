local anim8 = require "anim8/anim8"

local image, animation

function love.load()
	image = love.graphics.newImage('img/wolves/wolf_tailwag_tf-shadow.png')
	local g = anim8.newGrid(16, 16, image:getWidth(), image:getHeight())
	animation = anim8.newAnimation(g('1-3', 1), 0.1)
end
